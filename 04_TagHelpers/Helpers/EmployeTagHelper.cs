﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace _04_TagHelpers.Helpers
{
    [HtmlTargetElement("employe")]
    public class EmployeTagHelper : TagHelper
    {
        [HtmlAttributeName("name")]
        public string Name { get; set; } = String.Empty;

        [HtmlAttributeName("salary")]
        public string Salary { get; set; } = String.Empty;

        [HtmlAttributeName("type")]
        public string Type { get; set; } = String.Empty;

        [HtmlAttributeName("email")]
        public string Email { get; set; } = String.Empty;

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "div";
            output.Attributes.SetAttribute("class", "text-danger");
            output.TagMode = TagMode.StartTagAndEndTag;

            output.PreContent.SetHtmlContent($"<a href=mailto:{Email}>{Name}</a> est {Type} et gagne {Salary}€.");
        }
    }
}
