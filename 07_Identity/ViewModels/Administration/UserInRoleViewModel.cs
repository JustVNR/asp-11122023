﻿namespace _07_Identity.ViewModels.Administration
{
    public class UserInRoleViewModel
    {
        public string UserId { get; set; } = string.Empty;
        public string UserName { get; set; } = string.Empty;
        public bool IsSelected { get; set; }
    }
}
