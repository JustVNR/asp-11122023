﻿namespace _07_Identity.ViewModels.Administration
{
    public class UserRolesViewModel
    {
        /*public string UserId { get; set; } = String.Empty;*/
        public string RoleId { get; set; } = string.Empty;
        public string RoleName { get; set; } = string.Empty;
        public bool IsSelected { get; set; }
    }
}
