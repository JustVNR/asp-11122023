﻿using _02_PassingDataToView.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace _02_PassingDataToView.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            // Après être passé par la vue 'privacy' sans avoir été lue, on peut récupérer la donnée créée via un tempData
            if (TempData.ContainsKey("tempDataFromAction"))
            {
                ViewBag.tempDataFromAction = TempData["tempDataFromAction"]?.ToString();
            }

            if (TempData.ContainsKey("tempDataFromView"))
            {
                ViewBag.tempDataFromView = TempData["tempDataFromView"]?.ToString();
            }

            ViewBag.myCookie = Request.Cookies["key"];

            CookieOptions options = new()
            {
                Expires = DateTime.Now.AddSeconds(10)
            };

            Response.Cookies.Append("key", "cookie miam miam", options);

            Personnage p = new Personnage() { Prenom = "Riri", Nom = "Duck" };

            // La vue Index prend en paramètre un objet fortement typé de type "Personnage"
            return View(p);
        }

        public IActionResult Privacy()
        {
            /*
             * Viewbag permet de transférer des données du controleur à la vue
             * Ces données sont transférées en tant que propriété de l'objet Viewbag
             * La porté du viewbag est limitée à la requête actuelle : sa valeur est réinitialisée à null une fois transmise
             */

            ViewBag.Message = "your application privacy from viewbag";


            /*
             * ViewData est un objet de type dictionnaire permettant de transmettre des données du controller à la vue.
             * Ces données sont transférées sous forme de couple clé-valeur
             * La porté du ViewData est limitée à la requête actuelle : sa valeur est réinitialisée à null une fois transmise
             */
            ViewData["cle"] = "your application privacy from viewdata";

            IList<string> list = new List<string>()
            {
                "liste",
                "de chaines de caractères",
                "passée par ViewData"
            };

            ViewData["stringList"] = list;

            // ATTENTION ViewBag.toto <=> viewData["toto"] : Une propriété du ViewBag est reconnue en tant que clé du VieData et réciproquement

            /*
             * TempData peut être utilisé pour traansférer des donnes : 
             * - d'une vue à un controleur
             * - d'un controleur à une vue
             * - d'une action d'un contoleur à une autre action du même contoleur ou d'un contolueur différent.
             * 
             * TempDate stocke temporairement les données et les supprime après qu'elles aient été lues.
             */
            TempData["tempDataFromAction"] = "Data from Privacy in HomeController with tempData";

            // Session : 
            // - objet de type dictionnaire créé côté serveur
            // - accessible par l'ensemble des contôleurs et des vues
            // - expire par défaut au bout de 20min

            HttpContext.Session.SetString("user_name", "titi");
            HttpContext.Session.SetInt32("user_id", 12);

            // Peut être prématurément vidé de tout ou partie de son contenu
            //HttpContext.Session.Remove("user_id");
            //HttpContext.Session.Clear();

            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
