var builder = WebApplication.CreateBuilder(args);

//On s'abonne ici � l'ensmeble des services dont aura besoin l'application
builder.Services.AddControllersWithViews();

// On s'abonne au serviec de mise en cache
builder.Services.AddResponseCaching(options =>
{
    options.MaximumBodySize = 1024 * 1024;
});

// -----------------------------------------------------------------------------------------------------
// ----------------------------------------- Build de l'application ------------------------------------
// -----------------------------------------------------------------------------------------------------

var app = builder.Build();

// -----------------------------------------------------------------------------------------------------
// --------------------------------------- Configuration du pipeline -----------------------------------
// -----------------------------------------------------------------------------------------------------

// Le pipeline sp�cifie la mani�re dont l'application doit r�pondre � une requ�te.
// Qauand l'application re�oit une requ�te du client, la requ�te fait un aller retour dans le pipeline.
// Ce pipeline contient un ensemble de middlewares.
// Ces middleware permettent d'adresser diff�rents points techniques li�s � la requ�te :
// - la gestion des erreurs
// - la gestion des cookies
// - l'autehtification
// - la gestion des sessions
// - le routage
// - etc...

// Le passage de la requ�te � travers l'ensemble des middlewares permettra de cr�er une r�ponse.
// Les middlewares �tant �voqu�s s�quentiellement, il importe de les invoquer dans le bon ordre.
// Chaque middleware est charg� d'une t�che unique (single of responsability)


if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

//app.MapControllerRoute(
//    name: "default",
//    pattern: "{controller=New}/{action=ActionReturnString}/{id?}");

// MIDDLEWARE DE MISE EN CACHE
app.UseResponseCaching();

app.Run();
