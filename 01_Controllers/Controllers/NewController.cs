﻿using Microsoft.AspNetCore.Mvc;

namespace _01_Controllers.Controllers
{

    // [Route("new_route")]
    public class NewController : Controller
    {

        private readonly IWebHostEnvironment _webHostEnvironment;

        public NewController(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
        }

        // /new/ActionReturnString/
        // /new/ActionReturnString/?firstname=riri&lastname=duck
        // /new/ActionReturnString/2?firstname=riri&lastname=duck

        // [Route("new_action/{id?}")] // /new_route/new_action
        public string ActionReturnString(int? id)
        {
            string first = HttpContext.Request.Query["firstname"].ToString();
            string last = HttpContext.Request.Query["lastname"].ToString();
            return $"Hello from NewController with query strings firstname = {first} and lastname = {last} and id = {id} ";
        }

        // /new/ActionReturnSpecificView
        public ViewResult ActionReturnView()
        {
            return View(); // Retourne une vue portant le même nom que l'action courante dans le contoller courant
        }

        // /new/ActionReturnSpecificView

        public ViewResult ActionReturnSpecificView()
        {
            return View("SpecificView"); // Retourne une vue nommée 'SpecificView'
        }

        // /new/ActionReturnRedirectToAction
        public ActionResult ActionReturnRedirectToAction()
        {
            return RedirectToAction("ActionReturnString");
        }
        // /new/ActionReturnRedirectToActionWithParameters
        public ActionResult ActionReturnRedirectToActionWithParameters()
        {
            return RedirectToAction("ActionReturnString", new {id=99, firstname="toto"});
        }

        // /new/ActionReturnRedirectToRoute
        public ActionResult ActionReturnRedirectToRoute()
        {
            return RedirectToRoute(new { controller = "home", action = "privacy" });
        }

        // /new/ActionReturnJson
        public ActionResult ActionReturnJson()
        {
            return Json("{key:value, key2:{key3:value3}}");
        }

        // /new/ActionReturnContent
        public ActionResult ActionReturnContent()
        {
            return Content("<div>Contenu de ma div</div>", "text/html");
        }

        // /new/ActionReturnJavascript
        public ActionResult ActionReturnJavascript()
        {
            return Content("<script>alert('return Javascript')</script>", "text/html");
        }

        // /new/ActionReturnHttpStatusCode
        public ActionResult ActionReturnHttpStatusCode()
        {
            return StatusCode(StatusCodes.Status400BadRequest, "Mauvaise requête");
        }

        // /new/ActionReturnFilePathResult
        public FileResult ActionReturnFilePathResult()
        {
            string fileName = "site.css";

            string path = Path.Combine(_webHostEnvironment.WebRootPath, "css/", fileName);

            byte[] bytes = System.IO.File.ReadAllBytes(path);

            return File(bytes, "application/octet-stream", fileName);
        }


        // /new/ActionReturnFile // ATTENTION le nom de l'action ne contient pas le suffixe 'async'
        public async Task<FileResult> ActionReturnFileAsync()
        {
            string fileName = "site.css";

            string path = Path.Combine(_webHostEnvironment.WebRootPath, "css/", fileName);

            byte[] bytes = await System.IO.File.ReadAllBytesAsync(path);

            return File(bytes, "application/octet-stream", fileName);
        }
    }
}
