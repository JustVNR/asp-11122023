﻿using _01_Controllers.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

/*
 * 
 * Un controller est une classe chargé de contrôler la manière dont un utilisateur interagit avec une application MVC.
 * Il agit comme une gare de triage qui détermine le type de traitement à appliquer à la requête, et donc in fine la réponse à retourner.
 */
namespace _01_Controllers.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        // permet d'accéder à la configuration (appsettings.json, appsettings.developpement.json, user secrets, variable d'environnement...)
        private readonly IConfiguration _config;


        public HomeController(ILogger<HomeController> logger, IConfiguration config)
        {
            _logger = logger;
            
            _config = config;
        }

        [ResponseCache(Duration = 5)] // Mise en cache pendant 5 secondes
        public IActionResult Index()
        {
            _logger.LogTrace("Trace Log");
            _logger.LogDebug("Debug Log");
            _logger.LogInformation("Information Log");
            _logger.LogWarning("Warning Log");
            _logger.LogError("Error Log");
            _logger.LogCritical("Criticial Log");

            // throw new Exception("NEW EXEPTION TO TEST HANDLER EXCEPTION");

            return View("Index", _config.GetValue<string>("testSettings"));
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
