﻿using _05_Validations.Models;
using Microsoft.AspNetCore.Mvc;

namespace _05_Validations.Controllers
{
    public class EmployeController : Controller
    {
        private readonly IWebHostEnvironment _webHostEnvironment;


        public EmployeController(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
        }

        public ActionResult FormValidation()
        {
            return View(new Employe());
        }

        // https://fr.wikipedia.org/wiki/Cross-site_request_forgery
        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<IActionResult> FormValidation(Employe emp)
        {
            if (ModelState.IsValid)
            {
                string uploads = Path.Combine(_webHostEnvironment.WebRootPath, "uploads");

                if(emp.File is not null && emp.File.Length > 0)
                {
                    string fileName = Path.Combine(uploads, emp.File.FileName);

                    using (Stream fileStream = new FileStream(fileName, FileMode.Create))
                    {
                        await emp.File.CopyToAsync(fileStream);

                        return View("Details", emp);
                    }
                }
            }

            return View(emp);
        }
    }
}
