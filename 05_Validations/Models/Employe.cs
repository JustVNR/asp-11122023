﻿using System.ComponentModel.DataAnnotations;

namespace _05_Validations.Models
{
    public class Employe
    {
        [Required] //Champ obligatoire
        [Display(Name = "User Name")]
        [MaxLength(100)]
        public string UserName { get; set; } = String.Empty;

        [Required(ErrorMessage = "Mot de passe obligatoire")]
        [Display(Name = "Mot de passe")]
        [DataType(DataType.Password)]
        public string PassWord { get; set; } = String.Empty;

        [Required(ErrorMessage = "Date de naissance obligatoire")]
        [Display(Name = "Date de naissance")]
        [DataType(DataType.Date)]
        public DateTime DateNaissance { get; set; }

        [Required(ErrorMessage = "Email Obligatoire")]
        [EmailAddress(ErrorMessage = "Email invalide")]
        public string Email { get; set; } = String.Empty;

        [Required(ErrorMessage = "Evaluation Obligatoire")]
        [Range(1, 10)]
        public int Evaluation { get; set; }

        [Required(ErrorMessage = "Telephone Obligatoire")]
        [Display(Name = "Numéro de téléphone")]
        [RegularExpression(@"^(\d{10})$", ErrorMessage = "Le numéro de doit comporter exactement 10 chiffres.")]
        public string NumeroTelephone { get; set; } = String.Empty;

        [Required(ErrorMessage = "Commentaire Obligatoire")]
        [DataType(DataType.MultilineText)]
        public string Commentaire { get; set; } = String.Empty;

        [Required(ErrorMessage = "Avatar Obligatoire")]
        [Display(Name = "Uploader l'avatar")]
        [CustomAvatarValidation]
        public IFormFile? File { get; set; }
    }

    public class CustomAvatarValidationAttribute : ValidationAttribute
    {
        public override bool IsValid(object? value)
        {
            if (value is not IFormFile file)
            {
                return false;
            }

            int maxLength = 1024 * 1024;

            if (file.Length > maxLength) {
                ErrorMessage = $"File to large : maximum allowed size : {maxLength / 1024} MB";
                return false;
            }

            return true;
        }
    }
}
