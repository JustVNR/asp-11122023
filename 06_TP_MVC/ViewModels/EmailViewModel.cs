﻿using System.ComponentModel.DataAnnotations;

namespace _06_TP_MVC.ViewModels
{
    public class EmailViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name ="Your Email")]
        public string FromEmail { get; set; } = String.Empty;
        public string Subject { get; set; } = String.Empty;

        [DataType(DataType.MultilineText)]
        public string Body { get; set; } = String.Empty;

        [DataType(DataType.Upload)]
        public List<IFormFile>? Attachments { get; set; }
    }
}
