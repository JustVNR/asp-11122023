﻿using _06_TP_MVC.Models;

namespace _06_TP_MVC.ViewModels
{
    public class IndexViewModel<T>
    {
        public string Filter { get; set; } = String.Empty;

        public int PageSize { get; set; } = 10;

        public int TotalRecords { get; set; } = 0;

        public int CurrentPage { get; set; } = 1;

        public string OrderBy { get; set; } = String.Empty;

        public SortDirection Direction { get; set; } = SortDirection.Asc;

        public IList<T>? Items { get; set; }

        public SortDirection SwitchDirection()
        {
            return Direction == SortDirection.Asc ? SortDirection.Desc : SortDirection.Asc;
        }
    }

    public enum SortDirection
    {
        Asc,
        Desc
    }
}
