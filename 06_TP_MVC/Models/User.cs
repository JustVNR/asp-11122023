﻿using System.ComponentModel.DataAnnotations;

namespace _06_TP_MVC.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; } = String.Empty;

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; } = String.Empty;

        [Required]
        public bool IsAdmin { get; set; }

        [Range(1, int.MaxValue)]
        public int? Age { get; set; }
    }
}
