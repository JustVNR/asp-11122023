﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace _06_TP_MVC.Models
{
    public class Product
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Description { get; set; } = String.Empty;

        [Required]
        [DataType(DataType.Currency)]
        [Column(TypeName = "decimal(18,2)")]
        public decimal Prix { get; set; }

        // Entity Framework a besoin d'un constructeur sans paramètre (NON... ce n'est plus le cas au moins à tartir de EF CORE 7)
        public Product()
        {
            
        }
        public Product(int id, string description, decimal prix)
        {
            Id = id;
            Description = description;
            Prix = prix;
        }
    }
}
