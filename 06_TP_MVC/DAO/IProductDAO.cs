﻿using _06_TP_MVC.Models;
using _06_TP_MVC.ViewModels;

namespace _06_TP_MVC.DAO
{
    public interface IProductDAO
    {
        //Task<List<Product>> GetAll(string? description);

        Task<IndexViewModel<Product>> GetAll(IndexViewModel<Product>? model = null);

        Task<Product?> GetById(int id);

        Task Create(Product product);

        Task Update(Product product);

        Task Delete(int id);
    }
}
