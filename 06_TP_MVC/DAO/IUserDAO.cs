﻿using _06_TP_MVC.Models;
using _06_TP_MVC.ViewModels;

namespace _06_TP_MVC.DAO
{
    public interface IUserDAO
    {
        //Task<List<User>> GetAll();

        Task<IndexViewModel<User>> GetAll(IndexViewModel<User>? model = null);

        Task<User?> GetById(int id);
        Task Create(User user);
        Task Update(User user);
        Task Delete(int id);
    }
}
