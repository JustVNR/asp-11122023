﻿using _06_TP_MVC.Models;
using _06_TP_MVC.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace _06_TP_MVC.DAO
{
    public class ProductDAO : IProductDAO
    {
        private readonly ApplicationDbContext _db;

        public ProductDAO(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task Create(Product product)
        {
            _db.Add(product);
            await _db.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            var product = await _db.Products.FindAsync(id);

            if (product is not null)
            {
                _db.Products.Remove(product);
                await _db.SaveChangesAsync();
            }
        }

        //public async Task<List<Product>> GetAll(string? description)
        //{
        //    if (description is null)
        //    {
        //        return await _db.Products.AsNoTracking().ToListAsync();
        //    }
        //    else
        //    {
        //        return await _db.Products.AsNoTracking().Where(x => x.Description.Contains(description)).ToListAsync();
        //    }
        //}

        public async Task<IndexViewModel<Product>> GetAll(IndexViewModel<Product>? model = null)
        {
            model ??= new IndexViewModel<Product>();

            model.Filter ??= String.Empty;

            model.TotalRecords = await _db.Products.AsNoTracking().Where(x => x.Description.Contains(model.Filter)).CountAsync();

            model.Items = await _db.Products.AsNoTracking()
                    .Where(x => x.Description.Contains(model.Filter))
                    .OrderBy(model.OrderBy, model.Direction) // Méthode d'extension définie dans la classe "Extensions"
                    .Skip((model.CurrentPage - 1) * model.PageSize)
                    .Take(model.PageSize)
                    .ToListAsync();

            return model;
        }

        public async Task<Product?> GetById(int id)
        {
            //return await _db.Products.FirstOrDefaultAsync(m => m.Id == id);
            return await _db.Products.FindAsync(id); // plus efficace car recherche directement par rapport à la clé
        }

        public async Task Update(Product product)
        {
            Product? p = await _db.Products.FindAsync(product.Id);

            if (p is not null)
            {
                _db.Entry(p).State = EntityState.Detached;
                _db.Update(product);
                await _db.SaveChangesAsync();
            }
        }
    }
}
