﻿using _06_TP_MVC.Models;
using _06_TP_MVC.ViewModels;
using System.Linq.Expressions;

namespace _06_TP_MVC.DAO
{
    public static class Extensions
    {
        public static IQueryable<Product> OrderBy(this IQueryable<Product> query, string orderBy, SortDirection direction = SortDirection.Asc)
        {

            //Expression<Func<Product, object>> lambda;

            //switch (orderBy)
            //{
            //    case "description":
            //        lambda = x => x.Description;
            //        break;

            //    case "prix":
            //        lambda = x => x.Prix;
            //        break;

            //    default:
            //        lambda = x => x.Description;
            //        break;
            //}

            Expression<Func<Product, object>> lambda = orderBy?.ToLower() switch // NE PAS OUBLIER ?.ToLower() le point d'interrogation
            {
                "description" => x => x.Description,
                "prix" => x => x.Prix,
                _ => x => x.Description,
            };

            return direction == SortDirection.Asc ? query.OrderBy(lambda) : query.OrderByDescending(lambda);
        }

        public static IQueryable<User> OrderBy(this IQueryable<User> query, string orderBy, SortDirection direction = SortDirection.Asc)
        {

            Expression<Func<User, object>> lambda = orderBy?.ToLower() switch
            {
                "email" => x => x.Email,
                "age" => x => x.Age,
                _ => x => x.Email,
            };

            return direction == SortDirection.Asc ? query.OrderBy(lambda) : query.OrderByDescending(lambda);
        }
    }
}
