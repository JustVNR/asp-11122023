﻿using _06_TP_MVC.Models;
using _06_TP_MVC.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace _06_TP_MVC.DAO
{
    public class UserDAO : IUserDAO
    {
        private readonly ApplicationDbContext _db;

        public UserDAO(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task Create(User user)
        {
            _db.Add(user);
            await _db.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            var user = await _db.Users.FindAsync(id);

            if (user != null)
            {
                _db.Users.Remove(user);
                await _db.SaveChangesAsync();
            }
        }

        //public async Task<List<User>> GetAll()
        //{
        //    return await _db.Users.AsNoTracking().ToListAsync();
        //}

        public async Task<IndexViewModel<User>> GetAll(IndexViewModel<User>? model = null)
        {
            model ??= new IndexViewModel<User>();

            model.Filter ??= String.Empty;

            model.TotalRecords = await _db.Users.AsNoTracking().Where(x => x.Email.Contains(model.Filter)).CountAsync();

            model.Items = await _db.Users.AsNoTracking()
                    .Where(x => x.Email.Contains(model.Filter))
                    //.OrderBy(model.OrderBy, model.Direction) // Méthode d'extension définie dans la classe "Extensions"
                    .Skip((model.CurrentPage - 1) * model.PageSize)
                    .Take(model.PageSize)
                    .ToListAsync();

            return model;
        }

        public async Task<User?> GetById(int id)
        {
            return await _db.Users.FindAsync(id);
        }

        public async Task Update(User user)
        {
            _db.Update(user);
            await _db.SaveChangesAsync();
        }
    }
}
