using _06_TP_MVC.DAO;
using _06_TP_MVC.Models;
using _06_TP_MVC.Services;
using _06_TP_MVC.Settings;
using Microsoft.EntityFrameworkCore;
using NLog.Extensions.Logging;

var builder = WebApplication.CreateBuilder(args);

builder.Logging.AddNLog();


// Add services to the container.
builder.Services.AddControllersWithViews();

string? connectionString = builder.Configuration.GetConnectionString("ApplicationDbContext");

builder.Services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(connectionString));


builder.Services.Add(new ServiceDescriptor(typeof(EmailSettings),
    c => builder.Configuration.GetSection("MailSettings").Get<EmailSettings>(), ServiceLifetime.Singleton));


/* https://stackoverflow.com/questions/38138100/addtransient-addscoped-and-addsingleton-services-differences
 * - Transient  : objects are always different : a new instance is provided to every controller and every service.
 * - Scoped : objects are the same within a request, but different across different requests.
 * - Singleton : objects are the same for every object and every request.
 * https://youtu.be/01C8selSVCY
 * https://youtu.be/v6Nr7Zman_Y
 * 
 * Use cases : 
 * - https://exceptionnotfound.net/dependency-injection-in-dotnet-6-service-lifetimes/
 * - https://endjin.com/blog/2022/09/service-lifetimes-in-aspnet-core
 */


builder.Services.AddScoped<IEmailService, EmailService>();

builder.Services.AddScoped<IProductDAO, ProductDAO>();
builder.Services.AddScoped<IProductService, ProductService>();
builder.Services.AddScoped<IUserDAO, UserDAO>();
builder.Services.AddScoped<IUserService, UserService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();

    app.UseStatusCodePagesWithReExecute("/Error/{0}");
}

app.UseHttpsRedirection();

//app.UseRequestLocalization("fr-FR");
app.UseRequestLocalization("en-US");

app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
