﻿using _06_TP_MVC.ViewModels;

namespace _06_TP_MVC.Services
{
    public interface IEmailService
    {
        Task SendEmailAsync(EmailViewModel mailRequest);
    }
}
