﻿using _06_TP_MVC.Models;
using _06_TP_MVC.ViewModels;

namespace _06_TP_MVC.Services
{
    public interface IProductService
    {
        //Task<List<Product>> GetAll(string? description = null);
        Task<IndexViewModel<Product>> GetAll(IndexViewModel<Product>? model = null);

        Task<Product?> GetById(int id);

        Task Create(Product product);

        Task Update(Product product);

        Task Delete(int id);
    }
}
