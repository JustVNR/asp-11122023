﻿using System.Security.Cryptography;
using System.Text;

namespace _06_TP_MVC.Tools
{
    public class Hasher
    {

        public static string HashPassword(string pwd)
        {
            using(SHA256 sha256Hash = SHA256.Create())
            {
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(pwd));

                StringBuilder builder = new();

                for(int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("X2"));
                }

                return builder.ToString();
            }
        }
    }
}
