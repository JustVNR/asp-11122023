﻿using Microsoft.AspNetCore.Mvc;
using _06_TP_MVC.ViewModels;
using System.Diagnostics;
using Microsoft.AspNetCore.Diagnostics;

namespace _06_TP_MVC.Controllers
{
    public class ErrorController : Controller
    {
        private readonly ILogger<ErrorController> _logger;

        public ErrorController(ILogger<ErrorController> logger)
        {
            _logger = logger;
        }

        [Route("error/{statusCode}")]
        public IActionResult HttpStatusCodeHandler(int statusCode)
        {
            var statusCodeResult = HttpContext.Features.Get<IStatusCodeReExecuteFeature>();

            switch(statusCode)
            {
                case 404:
                    ViewBag.ErrorMessage = "Sorry, the resource you requested cound not be found";
                    ViewBag.Path = statusCodeResult?.OriginalPath;
                    ViewBag.QS = statusCodeResult?.OriginalQueryString;

                    _logger.LogWarning($"404 Error Occured. Path : {statusCodeResult?.OriginalPath}");

                    return View("NotFound");
                case 400:
                    ViewBag.ErrorMessage = "BAD REQUEST";
                    return View("NotFound");

                default:
                    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
            }
        }
    }
}
