﻿using _06_TP_MVC.Models;
using _06_TP_MVC.Services;
using _06_TP_MVC.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace _06_TP_MVC.Controllers
{
    public class ProductsController : Controller
    {
        private readonly IProductService _service;

        public ProductsController(IProductService service)
        {
            _service = service;
        }

        public async Task<IActionResult> Index()
        {
            return View((await _service.GetAll()).Items);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id is null)
            {
                return new StatusCodeResult(StatusCodes.Status400BadRequest);
            }

            Product? product = await _service.GetById((int)id);

            if (product is null)
            {
                //Response.StatusCode = 404;
                //return View("ProductNotFound", id);
                return new StatusCodeResult(StatusCodes.Status404NotFound);
            }

            return View(product);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Description,Prix")] Product product)
        {
            if (ModelState.IsValid)
            {
                await _service.Create(product);
                return RedirectToAction(nameof(Index));
            }
            return View(product);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id is null)
            {
                return new StatusCodeResult(StatusCodes.Status400BadRequest);
            }

            Product? product = await _service.GetById((int)id);

            if (product is null)
            {
                Response.StatusCode = 404;
                return View("ProductNotFound", id);
            }
            return View(product);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Description,Prix")] Product product)
        {
            if (ModelState.IsValid)
            {
                await _service.Update(product);
                return RedirectToAction(nameof(Index));
            }
            return View(product);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id is null)
            {
                return new StatusCodeResult(StatusCodes.Status400BadRequest);

            }

            Product? product = await _service.GetById((int)id);

            if (product is null)
            {
                Response.StatusCode = 404;
                return View("ProductNotFound", id);
            }

            return View(product);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {

            Product? product = await _service.GetById((int)id);

            if (product is not null)
            {
                await _service.Delete(id);
            }

            return RedirectToAction(nameof(Index));
        }


        // -------------------------------------------------
        // --------------------- AJAX FILTERED -------------
        // -------------------------------------------------

        public async Task<IActionResult> IndexFiltered()
        {
            return View((await _service.GetAll()).Items);
        }

        public async Task<IActionResult> _GetByDescription(string desc)
        {

            IndexViewModel<Product> vm = new()
            {
                Filter = desc,
                PageSize = 5
            };

            return PartialView("_ProductsPage", (await _service.GetAll(vm)).Items);
        }

        // -----------------------------------------------------
        // ----------- AJAX FILTERED UNOBSTRUSIVE --------------
        // -----------------------------------------------------

        public async Task<IActionResult> IndexFilteredUnobstrusive()
        {
            return View(await _service.GetAll(new IndexViewModel<Product>()));
        }

        public async Task<IActionResult> _IndexPartial(IndexViewModel<Product> vm)
        {
            return PartialView("_ProductsPage", (await _service.GetAll(vm)).Items);
        }

        // -----------------------------------------------------
        // --------------------- PAGINATION --------------------
        // -----------------------------------------------------

        public async Task<IActionResult> IndexPagined([FromQuery] int p = 1, [FromQuery] int s = 2, [FromQuery] string q = "")
        {
            return View(await _service.GetAll(new IndexViewModel<Product>()
            {
                Filter = q,
                PageSize = s,
                CurrentPage = p
            }));
        }

        // -----------------------------------------------------
        // --------------------- AJAX PAGINED ------------------
        // -----------------------------------------------------

        public async Task<IActionResult> IndexAjaxPagined([FromQuery] int p = 1, [FromQuery] int s = 2, [FromQuery] string q = "")
        {
            var model = await _service.GetAll(new IndexViewModel<Product>()
            {
                Filter = q,
                PageSize = s,
                CurrentPage = p
            });

            var isAjax = Request.Headers["X-Requested-With"] == "XMLHttpRequest";

            if (isAjax)
            {
                return PartialView("_IndexPartialPagined", model);
            }
            // else...
            return View(model);
        }

        // -----------------------------------------------------
        // -------------- PAGINATION AND SORTING ---------------
        // -----------------------------------------------------

        public async Task<IActionResult> IndexPaginedAndSorted(
           [FromQuery] int p = 1,
           [FromQuery] int s = 3,
           [FromQuery] string q = "",
           [FromQuery] string sort = "description",
           [FromQuery] SortDirection direction = SortDirection.Desc)
        {
            var model = await _service.GetAll(new IndexViewModel<Product>()
            {
                Filter = q,
                PageSize = s,
                CurrentPage = p,
                OrderBy = sort,
                Direction = direction
            });

            return View(model);
        }
    }
}
