﻿using _06_TP_MVC.DAO;
using _06_TP_MVC.Models;
using _06_TP_MVC.Services;
using _06_TP_MVC.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace _06_TP_MVC.Controllers
{
    public class UsersController : Controller
    {
        private readonly IUserService _service;

        public UsersController(IUserService service)
        {
            _service = service;
        }

        public async Task<IActionResult> Index()
        {
            return View((await _service.GetAll()).Items);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id is null)
            {
                return new StatusCodeResult(StatusCodes.Status400BadRequest);
            }

            User? user = await _service.GetById((int)id);

            if (user is null)
            {
                Response.StatusCode = 404;
                return View("UserNotFound", id);
            }

            return View(user);

        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(User user)
        {
            if (ModelState.IsValid)
            {
                await _service.Create(user);
                return RedirectToAction(nameof(Index));
            }
            return View(user);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id is null)
            {
                return new StatusCodeResult(StatusCodes.Status400BadRequest);
            }

            User? user = await _service.GetById((int)id);

            if (user is null)
            {
                Response.StatusCode = 404;
                return View("UserNotFound", id);
            }
            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, User user)
        {
            if (ModelState.IsValid)
            {
                await _service.Update(user);
                return RedirectToAction(nameof(Index));
            }
            return View(user);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id is null)
            {
                return new StatusCodeResult(StatusCodes.Status400BadRequest);
            }

            User? user = await _service.GetById((int)id);

            if (user is null)
            {
                Response.StatusCode = 404;
                return View("UserNotFound", id);
            }

            return View(user);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            User? user = await _service.GetById((int)id);

            if (user is not null)
            {
                await _service.Delete(id);
            }

            return RedirectToAction(nameof(Index));
        }
        // -----------------------------------------------------
        // --------------------- PAGINATION --------------------
        // -----------------------------------------------------

        public async Task<IActionResult> IndexPaginedAndSorted(
            [FromQuery] int p = 1,
            [FromQuery] int s = 2,
            [FromQuery] string q = "",
            [FromQuery] string sort = "email",
            [FromQuery] SortDirection direction = SortDirection.Desc)
        {
            return View(await _service.GetAll(new IndexViewModel<User>()
            {
                Filter = q,
                PageSize = s,
                CurrentPage = p,
                OrderBy = sort,
                Direction = direction
            }));
        }
    }
}
