﻿using _06_TP_MVC.Services;
using _06_TP_MVC.ViewModels;
using LazZiya.TagHelpers.Alerts;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace _06_TP_MVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        private readonly IEmailService _mail;

        public HomeController(ILogger<HomeController> logger, IEmailService mail)
        {
            _logger = logger;

            _mail = mail;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpGet]
        public IActionResult Contact()
        {
            return View(new EmailViewModel());
        }

        [HttpPost]
        public async Task<IActionResult> Contact(EmailViewModel mail)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _mail.SendEmailAsync(mail);
                    TempData["success"] = "Message sent successfully"; // Notif Toastr Success

                    // LazZia TagHelpers
                    TempData.Success("Message sent successfully");

                    return View("Index");

                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    TempData["error"] = "Message could not be sent"; // Notif Toastr Error

                    // LazZia TagHelpers
                    TempData.Danger("Message cound not be sent");
                }
            }

            return View(mail);
        }
    }
}
